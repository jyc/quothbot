(use irc args irregex posix srfi-1 srfi-13 srfi-18 srfi-12 srfi-28 srfi-69)
(include "load.scm")

(import (prefix quoth quoth:))

; depends: args (egg), ports (chicken unit), parameters (chicken builtin),
; keyword arguments (chicken bulitin), library (chicken unit), format
; (srfi 28), data-structures (chicken unit)
(define-syntax parse-options!
  (syntax-rules ()
    ((_ ((long-key short-key description default transform) ...))
     (begin
       (define options #f)
       (define operands #f)
       (define (usage)
         (with-output-to-port (current-error-port)
           (lambda ()
             (print "Usage: " (car (argv)) " [options...]")
             (newline)
             (print (args:usage opts))))
         (exit 1))
       (define opts (list
                      (if default
                        (args:make-option (short-key long-key) #:optional (format "~a [default: ~a]" description default))
                        (args:make-option (short-key long-key) #:required description))
                      ...
                      (args:make-option (h help) #:none "display this text" (usage))))
       (define transformeds '())
       (call-with-values (lambda () (args:parse (command-line-arguments) opts))
                         (lambda (a b)
                           (set! options a)
                           (set! operands b)))
       (let* ((transformer (lambda (long-key) transform))
              (transformed (transformer (alist-ref 'long-key options eq? default))))
         (if (null? transformed)
           (usage)
           (set! transformeds (cons (cons 'long-key transformed) transformeds))))
       ...
       (define long-key (make-parameter (alist-ref 'long-key transformeds)))
       ...))))

(args:width 30)

(parse-options! ((server
                   s "host to connect to" "127.0.0.1"
                   (if server server '()))
                 (port
                   p "port to connect to" "6667"
                   (let ((n (and port (string->number port))))
                     (if n n '())))
                 (nick
                   n "nickname and username to use" "quothbot"
                   (if (and nick (> (string-length nick) 0)) nick '()))
                 (channel
                   c "channel to join" #f
                   (if (and channel (> (string-length channel)) 0) channel '()))
                 (input-path
                   c "input file" #f
                   (if (and input-path (> (string-length input-path)) 0) input-path '()))
                 ))

(define con (irc:connection
              server: (server)
              port: (port)
              user: (nick)
              nick: (nick)))

(define old-exception-handler (current-exception-handler))
(current-exception-handler (lambda (exn)
                               (irc:quit con "oups! :[")
                               (old-exception-handler exn)
                               (exit 1)))

(set-signal-handler! signal/int (lambda (signum)
                                  (current-exception-handler old-exception-handler)
                                  (irc:quit con "cya.")
                                  (exit 1)))

(irc:connect con)

; join the channel after receiving 001 (welcome)
(irc:add-message-handler!
  con (lambda (msg)
        (irc:join con (channel)))
  command: "001")

(define corpus (quoth:load-corpus (input-path)))

(let ((query (lambda (line)
               (let-values (((response score) (quoth:query-corpus corpus line)))
                 (if (> score 0)
                   (values response score)
                   (values (quoth:corpus-random-sentence corpus) 0))))))
  (irc:add-message-handler!
    con
    (lambda (raw-message)
      (if (not (irc:extended-data? (second (irc:message-parameters raw-message))))
        (let*-values (((sender) (irc:message-sender raw-message))
                      ((message) (second (irc:message-parameters raw-message)))
                      )
          (if (> (string-length message) (+ (string-length (nick)) 2))
            (let-values (((response score) (query (string-drop message (+ (string-length (nick)) 2)))))
              (when (and (or (string-prefix? (string-append (nick) ": ") message)
                             (string-prefix? (string-append (nick) ", ") message)))
                (irc:say con (string-append sender ": " response) (channel))))))))
    command: "PRIVMSG"))

(irc:connection-raw-filter-set! con
                                (lambda (input)
                                  (case (string->number (list-ref (irregex-split '(" ") input) 1))
                                    ((422) "") ; ignore 422 messages (no MOTD) 'errors' - it causes the irc bot to
                                    ; shut down even when it shouldn't
                                    (else input))))

(irc:run-message-loop con
                      debug: #f
                      pong: #t)
