CFLAGS=-O2
MAIN=main.scm
LOAD=load.scm
EXECUTABLE=quothbot
LIBRARY=quothbot.so
DEPLOY=deploy
EGGS=ssax input-parse html-parser sxpath irc regex matchable args srfi-37 vector-lib

.PHONY: clean interpret test

all: $(EXECUTABLE)

interpret:
	csi $(MAIN)

deploy: $(MAIN) $(LOAD)
	csc $(CFLAGS) -deploy $(MAIN) -o $(DEPLOY)
	chicken-install -deploy -p $(DEPLOY) $(EGGS)
	mv ${DEPLOY}/${DEPLOY} ${DEPLOY}/${EXECUTABLE}

$(EXECUTABLE): $(MAIN) $(LOAD)
	csc $(CFLAGS) $(MAIN) -o $@

lib: $(LOAD)
	csc $(CFLAGS) -s $(LOAD) -o $(LIBRARY)

clean:
	rm -rf ${DEPLOY}
	rm -f $(EXECUTABLE) 
	rm -f $(LIBRARY)

test:
	csi tests/run.scm
