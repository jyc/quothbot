(use srfi-1 srfi-13)
(use list-utils)
(include "load.scm")
(import quoth)

(define macbeth-in (open-input-file "macbeth.html"))
(define macbeth (parse-macbeth macbeth-in))
(close-input-port macbeth-in)

(define caesar-in (open-input-file "caesar.html"))
(define caesar (parse-macbeth caesar-in))
(close-input-port caesar-in)

; want:
; - latin
;   - english (*)
;     - source of english
; do not want:
; - english quote about seneca
;   - source of english quote about seneca
(define seneca-in (open-input-file "seneca.html"))
(define seneca (parse-wikiquotes seneca-in
                                 xpath: "//div[@id='mw-content-text']/ul/li/ul/li[1]"
                                 mutator: (lambda (x)
                                            (if (ormap (lambda (elem)
                                                         (and (pair? elem) (eq? (car elem) 'ul)))
                                                       x)
                                              (filter (lambda (elem)
                                                        (not (and (pair? elem) (eq? (car elem) 'ul))))
                                                      x)
                                              #f)) ; make sure itcontains a sublist)))
                                 ))

(define (parse-wikiquotes-until path prefix)
  ; want:
  ; - quote (*)
  ;     - source
  (define in (open-input-file path))
  (define reached? #f)
  (define corpus (parse-wikiquotes in
                                   xpath: "//div[@id='mw-content-text']/ul/li"
                                   mutator: (lambda (x)
                                              (if reached?
                                                #f
                                                (let ((fst (cadr x)))
                                                  (if (and (string? fst)
                                                           (string-prefix? prefix fst))
                                                    (begin
                                                      (set! reached? #t)
                                                      #f)
                                                    (filter (lambda (elem)
                                                              (not (and (pair? elem) (eq? (car elem) 'ul))))
                                                            x)))))))
  (close-input-port in)
  corpus)

;(define abe (parse-wikiquotes-until "abe.html" "The Illinois State Republican Convention met at Bloomington on May 29, 1856." ))

;(define washington (parse-wikiquotes-until "washington.html" "The duty of holding a neutral conduct may be inferred, without any thing more, from the obligation which justice and humanity impose on every nation, in cases in which it is free to act, to maintain inviolate the relations of peace and amity towards other nations."))

(define marcus (parse-wikiquotes-until "marcus.html" "A man's greatness lies not in wealth and station"))

(define mega (fold (lambda (a b) (merge-corpora a b)) macbeth (list caesar seneca marcus)))

(save-corpus mega "mega.scm")
