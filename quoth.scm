(module quoth (save-corpus parse-wikiquotes parse-srt parse-macbeth load-corpus query-corpus corpus-random-sentence merge-corpora)
  (import scheme chicken)
  (use srfi-1 srfi-13 srfi-69)
  (use data-structures extras ports irregex)
  (use ssax html-parser sxpath vector-lib matchable txpath)

  (include "stopwords")
  (define stopword?
    (let ((stopword-ht (make-hash-table)))
      (begin
        (for-each (lambda (stopword)
                    (hash-table-set! stopword-ht stopword #t))
                  stopwords)
        (lambda (word)
          (hash-table-exists? stopword-ht word)))))

  (define (split-line line)
    (irregex-split "\\s+" line))

  (define (line->normalized-words line)
    (filter (compose not stopword?) (delete-duplicates (map normalize-word (split-line line)))))

  (define (normalize-word word)
    (string-downcase
      (string-trim-both
        word
        (lambda (char)
          (member char '(#\' #\" #\: #\; #\. #\? #\, #\. #\!))))))

  (define (strip-tags sxml)
    (match sxml
      [(tag ('@ attrs ...) body ...)
       (string-join (map strip-tags body) "")]
      [(tag body ...)
       (string-join (map strip-tags body) "")]
      [text text]))

  (define (build-word-ht sentences associations)
    (define word-ht (make-hash-table))
    (define sentences-len (length sentences))
    (let loop ((i 0)
               (rest sentences))
      (when (pair? rest)
        (for-each (lambda (word)
                    (hash-table-update!/default
                      word-ht word
                      (lambda (old)
                        (append (filter (lambda (x) (and (>= x 0) (< x sentences-len)))
                                        (map (lambda (x) (+ x i)) associations))
                                old))
                      '()))
                  (line->normalized-words (car rest)))
        (loop (+ i 1) (cdr rest))))  
    word-ht)

  (define (build-sentences-from-words words)
    (reverse
      (map
        (lambda (line-parts)
          (string-join (reverse (cdr line-parts)) " ")) 
        (fold (lambda (word ls)
                (let ((is-sentence-terminator? (irregex-match "^.*[\\.\\?!]$" word)))
                  (if (or (null? ls) (caar ls)) ; mark the sublist start with #t if it is a completed sentence
                    (if (string=? word "/")
                      ls
                      (cons (list is-sentence-terminator? word) ls))
                    (cons (cons is-sentence-terminator? (cons word (cdar ls))) (cdr ls)))))
              '() words))))

  (define (strip-xml-tags str)
    (irregex-replace/all "</?.+>" str ""))

  (define (parse-srt input)
    (define (read-srt-datum input)
      (read-line input) ; the subtitle number
      (read-line input) ; the time range
      (let loop ((lines '()))
        (let ((line (strip-xml-tags (string-trim (read-line input)))))
          (if (string=? line "")
            (string-join (reverse lines) " ")
            (loop (cons line lines))))))

    (define words
      (reverse (fold (lambda (sentence words)
                       (append (reverse (split-line sentence)) words))
                     '()
                     (let loop ((sentences '()))
                       (if (eof-object? (peek-char input))
                         (reverse sentences)
                         (loop (cons (read-srt-datum input) sentences)))))))

    (define sentences (build-sentences-from-words words))

    (map (lambda (sentence)
           (set! sentence
             (if (string-prefix-ci? "<i>" sentence)
               (string-drop sentence 3)
               sentence))
           (set! sentence
             (if (string-suffix-ci? "</i>" sentence)
               (string-drop-right sentence 4)
               sentence))
           sentence)
         sentences)

    (make-corpus (build-word-ht sentences '(-1 0 1)) (list->vector sentences)))

  (define (parse-macbeth input)
    (define (is-speech-name? name)
      (if (irregex-match "^[0-9]+\\.[0-9]+\\.[0-9]+$" name) #t #f))

    (define sxml (html->sxml input))

    (define select-lines (sxpath '(// a)))

    (define words
      (reverse (fold (lambda (elem words)
                       (append (reverse (split-line (string-trim (caddr elem)))) '("/") words))
                     '()
                     (filter (lambda (elem)
                               (and
                                 (eq? '@ (caadr elem))
                                 (eq? 'name (car (cadadr elem)))
                                 (is-speech-name? (cadr (cadadr elem)))))
                             (select-lines sxml)))))

    (define sentences (build-sentences-from-words words))

    (make-corpus (build-word-ht sentences '(-1 0 1)) (list->vector sentences)))

  (define (parse-wikiquotes input #!key (xpath "//div[@id='mw-content-text']/ul/li") (mutator (lambda (x) x)))
    (define sxml (html->sxml input))
    (define quotes
      (filter (lambda (x) x) (map mutator ((txpath xpath) sxml))))

    (define words
      (reverse (fold (lambda (phrase words)
                          (append (reverse (split-line (string-trim phrase))) words)) 
                        '()
                        (map strip-tags quotes))))

    (define sentences (build-sentences-from-words words))

    (make-corpus (build-word-ht sentences '(0 1)) (list->vector sentences)))

  (define (save-corpus corpus output-path)
    (let ((out (open-output-file output-path)))
      (write (hash-table->alist (corpus-word-ht corpus)) out)
      (newline out)
      (write (corpus-sentences corpus) out)
      (close-output-port out)))

  (define (merge-corpora a b)
    (let ((b-offset (vector-length (corpus-sentences a)))
          (merged-ht (hash-table-copy (corpus-word-ht a))))
      (hash-table-for-each
        (corpus-word-ht b)
        (lambda (word b-subsequents)
          (hash-table-update!/default
            merged-ht word
            (lambda (a-subsequents)
              (append a-subsequents (map (lambda (id) (+ id b-offset)) b-subsequents)))
            '())))
      (make-corpus merged-ht (vector-append (corpus-sentences a) (corpus-sentences b)))))

  (define (read-sexpr-line)
    (call-with-input-string (read-line)
                            (lambda (port)
                              (read port))))

  (define-record corpus
    word-ht     ; SRFI-69 hash-table
    sentences   ; vector of strings

    )

  (define (load-corpus input-path)
    (with-input-from-file input-path
      (lambda ()
        (let ((word-ht (alist->hash-table (read-sexpr-line)))
              (lines (read-sexpr-line)))
          (make-corpus word-ht lines)))))

  (define (corpus-random-sentence corpus)
    (let ((sentences (corpus-sentences corpus)))
      (vector-ref sentences (random (vector-length sentences)))))

  (define (query-corpus corpus line)
    (let ((word-ht (corpus-word-ht corpus))
          (lines (corpus-sentences corpus))
          (words (line->normalized-words line))
          (scores (make-hash-table))
          (max-score 0)
          (max-scorers '()))
      (for-each (lambda (word*)
                  (let ((word (normalize-word word*)))
                    (if (hash-table-exists? word-ht word)
                      (for-each (lambda (subsequent)
                                  (hash-table-update!/default scores subsequent add1 0)
                                  (let ((new-score (hash-table-ref scores subsequent)))
                                    (cond
                                      ((> new-score max-score)
                                       (set! max-score new-score)
                                       (set! max-scorers (list subsequent)))
                                      ((= new-score max-score)
                                       (set! max-scorers (cons subsequent max-scorers))))))
                                (hash-table-ref word-ht word)))))
                words)
      (values (if (> max-score 0)
                (vector-ref lines (list-ref max-scorers (random (length max-scorers))))
                #f)
              max-score)))

  )
